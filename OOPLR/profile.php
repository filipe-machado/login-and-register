<?php

require_once 'Vendor/core/Autoload.php';

if (!$username = Input::get('user'))
{
    Redirect::to('index');
}
else
{
    $user = new User($username);
    if (!$user->exists())
    {
        Redirect::to(404);
    }
    else
    {
        $data = $user->data();
    }
    ?>
    <h3><?= escape($data->username) ?></h3>
    <p>Nome completo: <?= escape($data->name) ?></p>
    <?php
}
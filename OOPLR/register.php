<?php

require_once 'Vendor/core/Autoload.php';

if (Input::exists())
{
    if (Token::check(Input::get('token')))
    {
        $validate = new Validate;
        $validation = $validate->check($_POST, 
        [
            // Dados possíveis em HTML passados via PHP para evitar manipulação
            'username' => [
                'required' => true, // Input obrigatório
                'min' => 2, // Número mínimo de caracteres permitidos na coluna username
                'max' => 64, // Número máximo de caracteres permitidos na coluna username
                'unique' => 'users' // Verifica se o usuário será único
            ],
            'password' => [
                'required' => true,
                'min' => 6
            ],
            'password_again' => [
                'required' => true,
                'matches' => 'password' // Verifica se é igual a password
            ],
            'name' => [
                'required' => true,
                'min' => 2,
                'max' => 50
            ]
        ]);

        if ($validation->passed())
        {
            $user = new User;

            $salt = Hash::salt(32);

            try
            {
                $user->create([
                    'username' => Input::get('username'),
                    'password' => Hash::make(Input::get('password'), $salt),
                    'salt' => $salt,
                    'name' => Input::get('name'),
                    'joined' => date('Y-m-d H:i:s'),
                    'group' => 1,
                ]);

                Session::flash('home', 'Você foi registrado e pode efetuar LogIn');
                Redirect::to('index');
            }
            catch (Exception $e)
            {
                die($e->getMessage());
            }
        }
        else
        {
            foreach ($validation->errors() as $error)
            {
                echo $error , "<br>";
            }
        }
    }
}

?>

<form action="" method="post">
    <div class="field">
        <label for="username">Username</label>
        <input type="text" name="username" id="username" value="<?= escape(Input::get('username')) ?>" autocomplete="off">
    </div>

    <div class="field">
        <label for="password">Password</label>
        <input type="password" name="password" id="password">
    </div>

    <div class="field">
        <label for="password_again">Password Again</label>
        <input type="password" name="password_again" id="password_again">
    </div>

    <div class="field">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?= escape(Input::get('name')) ?>" autocomplete="off">
    </div>

    <div class="field">
        <input type="hidden" name="token" value="<?= Token::generate() ?>">
    </div>

    <div class="field">
        <input type="submit" value="Register">
    </div>
</form>
<?php

// namespace InnerIn\Interop;

interface DBInterface
{
    /**
     * Retorna todos os registros da tabela.
     * 
     * @param string $table seleciona uma tabela;
     * @param string $where seleciona o/os campos de consulta.
     * 
     * @return array
     */
    public function get($table, $where);
    
    /**
     * Insere dados em uma tabela do banco de dados.
     * 
     * @param string $table seleciona uma tabela;
     * @param array $fields insere os valores nas colunas da tabela.
     */
    public function insert($table, $fields = []);
    
    /**
     * Atualiza dados dos registros da tabela.
     * 
     * @param string $table seleciona uma tabela;
     * @param int $id seleciona o id alvo da consulta;
     * @param array $fields passa os campos para atualização por binding.
     */
    public function update($table, $id, $fields);

    /**
     * Deleta registros da tabela.
     * 
     * @param string $table seleciona uma tabela;
     * @param string $where seleciona o alvo a ser deletado.
     */
    public function delete($table, $where);
}
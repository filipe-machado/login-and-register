<?php

class Input
{
    /**
     * Função estática que verifica qual método está sendo passado.
     */
    public static function exists($type = 'post')
    {
        switch ($type)
        {
            case 'post':
                return (!empty($_POST)) ? true : false; 
            break;
            case 'get':
                return (!empty($_GET)) ? true : false;
            break;
            default:
                return false;
            break;
        }
    }

    /**
     * Função que verifica qual parâmetro está sendo passado 
     * na variável global $_POST ou $_GET
     */
    public static function get($item)
    {
        if (isset($_POST[$item]))
        {
            return $_POST[$item];
        }
        elseif (isset($_GET[$item]))
        {
            return $_GET[$item];
        }
        return '';
    }
}
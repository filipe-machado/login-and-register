<?php

require_once 'Vendor/core/Autoload.php';

if (Input::exists())
{
    if (Token::check(Input::get('token')))
    {
        $validate = new Validate;
        $validation = $validate->check($_POST, 
        [
            'username' => [
                'required' => true
            ],
            'password' => [
                'required' => true
            ]
        ]);

        if ($validation->passed())
        {
            $user = new User;

            $remember = (Input::get('remember') === 'on') ? true : false;
            $login = $user->login(Input::get('username'), Input::get('password'), $remember);

            if ($login)
            {
                Redirect::to('index');
            }
            else
            {
                echo 'Não foi possível realizar login';
            }
        }
        else
        {
            foreach ($validation->errors() as $error)
            {
                echo $error , "<br>";
            }
        }
    }
}

?>

<form action="" method="post">
    <div class="field">
        <label for="username">Username</label>
        <input type="text" name="username" id="username" autocomplete="off">
    </div>
    
    <div class="field">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" autocomplete="off">
    </div>

    <div class="field">
        <input type="checkbox" name="remember" id="remember"> 
        <label for="remember">Lembrar-se</label>
    </div>

    <div class="field">
        <input type="hidden" name="token" id="token" value="<?=Token::generate()?>">
        <input type="submit" value="LogIn">
    </div>
</form>
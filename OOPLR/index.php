<?php

require_once 'Vendor/core/Autoload.php';

if (Session::exists('home'))
{
    echo '<p>' . Session::flash('home') . '</p>';
}

$user = new User();
if($user->isLoggedIn())
{?>

    <p>Olá, <a href="profile.php?user=<?= escape($user->data()->username); ?>"><?= escape($user->data()->name); ?></a>!</p>

    <ul>
        <li><a href="logout.php">Log Out</a></li>
        <li><a href="update.php">Update</a></li>
        <li><a href="changepassword.php">Change password</a></li>
    </ul>

<?php
    if ($user->hasPermission('admin'))
    {
        echo "Você é administrador";
    }
}
else
{
    echo "Você precisa se <a href='login.php'>logar</a> ou criar uma <a href='register.php'>nova conta</a>";
}
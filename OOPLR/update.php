<?php

require_once 'Vendor/core/Autoload.php';

$user = new User;

if (!$user->isLoggedIn())
{
    Redirect::to('index');
}

if (Input::exists())
{
    if (Token::check(Input::get('token')))
    {
        $validate = new Validate;
        $validation = $validate->check($_POST, [
            'name' => [
                'required' => true,
                'min' => 2,
                'max' => 64
            ]
        ]);

        if ($validation->passed())
        {
            try
            {
                $user->update([
                    'name' => Input::get('name')
                ]);
                Session::flash('home', 'Seu usuário foi atualizado com sucesso!');
                Redirect::to('index');
            }
            catch (Exception $e)
            {
                die($e->getMessage());
            }
        }
        else
        {
            foreach ($validation->errors() as $error)
            {
                echo $error, "<br>";
            }
        }
    }
}

?>

<form action="" method="post">
    <div class="field">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?= escape($user->data()->name) ?>">

        <input type="submit" value="Update">
        <input type="hidden" name="token" value="<?= Token::generate() ?>">
    </div>
</form>
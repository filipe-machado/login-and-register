<?php

require_once 'Vendor/core/Autoload.php';

$user = new User;
$user->logout();

Redirect::to('index');
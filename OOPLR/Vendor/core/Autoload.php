<?php

################################################################
##  Core do sistema, onde serão carregados todos os arquivos. ##
#### Todos os arquivos da pasta Vendor são carregados aqui. ####
################################################################

include_once("init.php");

class Autoload
{
    private static $_instance;
    private $_db;

    /**
     * Strategy Pattern
     */
    public function getInstance()
    {
        if (!isset(self::$_instance))
        {
            self::$_instance = new Autoload;
        }
        return self::$_instance;
    }

    public function loadFiles()
    {
        spl_autoload_register(function($class)
        {
            $bar = dirname(dirname(dirname(__FILE__)));
            $pathfile = "$bar\\";
            
            $paths = array(
                'System\\',
                'System\\App\\',
                'System\\App\\classes\\',
                'System\\App\\Interfaces\\inner-exception\\Exception\\',
                'System\\App\\Interfaces\\inner-interface\\interop\\',
                'System\\includes\\'
            );

            // Varre o array $paths lendo os caminhos
            foreach($paths as $bit)
            {
                if (file_exists($pathfile . $bit . $class . '.php'))
                {
                    require_once($pathfile . $bit . $class . '.php');
                }
            }
        });
    }
}

$load = new Autoload;
$load->getInstance()->loadFiles();

if (Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name')))
{
    $hash = Cookie::get(Config::get('remember/cookie_name'));
    $hashCheck = DB::getInstance()->get('users_session', ['hash', '=', $hash]);

    if ($hashCheck->count())
    {
        $user = new User($hashCheck->first()->user_id);
        $user->login();
    }
}   